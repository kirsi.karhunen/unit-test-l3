// example.test.js
const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {
    let myvar = undefined;

    // runs after all tests in this block
    after(() => console.log('After mylib tests'));

    // unit test for the mylib.sum function. Expect
    it('Should return 2 when using sum function with a=myvar, b=1', () => {
        const result = mylib.sum(1, 1); // 1 + 1
        expect(result).to.equal(2); // result expected to equal 2
    });
    // run tasks before tests in this block
    before(() => {
        myvar = 1; // setup before testing
        console.log('Before testing');
    });
    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar');
    })
    it('Myvar should exist', () => {
        should.exist(myvar)
    })
    
});